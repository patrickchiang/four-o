module.exports = function (sequelize, Sequelize) {
    var UserInfo = sequelize.define('UserInfo', {
        info_first_name: {
            type: Sequelize.STRING
        },
        info_last_name: {
            type: Sequelize.STRING
        },
        info_bio: {
            type: Sequelize.STRING,
            allowNull: true
        },
        info_picture: {
            type: Sequelize.STRING,
            allowNull: true
        },
        info_major: {
            type: Sequelize.STRING
        }
    }, {
        freezeTableName: true,
        classMethods: {
            associate: function (models) {
                UserInfo.belongsTo(models.User);
            }
        }
    });

    return UserInfo;
};