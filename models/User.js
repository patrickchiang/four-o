module.exports = function (sequelize, Sequelize) {
    var User = sequelize.define('User', {
        user_email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        user_username: {
            type: Sequelize.STRING,
            allowNull: true,   // TODO: change
            unique: true
        },
        user_password: {
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        freezeTableName: true,
        classMethods: {
            associate: function (models) {
                User.hasMany(models.UserInfo);
                User.hasMany(models.User_Course);
            }
        }
    });

    return User;
};