module.exports = function (sequelize, Sequelize) {
    var User_Course = sequelize.define('User_Course', {}, {
        freezeTableName: true,
        classMethods: {
            associate: function (models) {
                User_Course.belongsTo(models.User);
                User_Course.belongsTo(models.Course);
            }
        }
    });

    return User_Course;
};