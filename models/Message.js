module.exports = function (sequelize, Sequelize) {
    var Message = sequelize.define('Message', {
        message_text: {
            type: Sequelize.STRING
        }
    }, {
        freezeTableName: true,
        classMethods: {
            associate: function (models) {
                Message.belongsTo(models.User, {as: 'sender'});
                Message.belongsTo(models.User, {as: 'receiver'});
            }
        }
    });

    return Message;
};