module.exports = function (sequelize, Sequelize) {
    var Course = sequelize.define('Course', {
        course_department: {
            type: Sequelize.STRING,
            allowNull: false
        },
        course_number: {
            type: Sequelize.STRING,
            allowNull: true
        }
    }, {
        freezeTableName: true,
        classMethods: {
            associate: function (models) {
                Course.hasMany(models.User_Course);
            }
        }
    });

    return Course;
};