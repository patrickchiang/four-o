/**
 * Module imports
 */

var express = require('express');
var app = express();
var mysql = require('mysql');
var Sequelize = require('sequelize');

var models = require('./models');
var ensure = require('./routes/ensure.js');

/**
 * Routes
 */

require('./routes/auth.js')(app);
require('./routes/users.js')(app);
require('./routes/courses.js')(app);
require('./routes/users_courses.js')(app);
require('./routes/messages.js')(app);

/**
 * Views
 */

app.get('/', function (req, res, next) {
    if (!req.user) {
        res.redirect('/login.html');
    } else {
        res.redirect('/index.html');
    }
});

app.get('/index.html', ensure, function (req, res) {
    res.sendFile(__dirname + '/site/index.html');
});

app.get('/buddies.html', ensure, function (req, res) {
    res.sendFile(__dirname + '/site/buddies.html');
});

app.get('/init', function (req, res) {
    models.sequelize.sync({force: true});
    res.json('done')
});

/**
 * Static views
 */

app.use('/', express.static(__dirname + '/site'));

/**
 * Initialize server
 */

var server = app.listen(3000, 'localhost', function () {
    console.log('Listening on port %d', server.address().port);
});