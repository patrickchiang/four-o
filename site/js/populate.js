$(document).ready(function () {
    var courses = [], peopleList = [], people = [];

    var getCourses = $.get("/users_courses/courses", function (data) {
        courses = data;
    });

    var getPeopleList, getPeople;

    $.when(getCourses).done(function () {
        for (var i = 0; i < courses.length; i++) {
            getPeopleList = $.get("/users_courses/users/" + courses[i].course_department + "/" + courses[i].course_number, function (data) {
                for (var j = 0; j < data.length; j++) {
                    peopleList.push(data[j]);
                }
            });
        }

        $.when(getPeopleList).done(function () {
            peopleList = $.unique(peopleList);

            for (var i = 0; i < peopleList.length; i++) {
                getPeople = $.get("/userinfo/id/" + peopleList[i], function (data) {
                    people.push(data);
                });
            }

            $.when(getPeople).done(function () {
                console.log(people)

                for (var idx = 0; idx < people.length; ++idx) {
                    console.log("so many buts");

                    var modal = $("#Student_List").clone();

                    modal.css("display", "block");
                    modal.appendTo($("#accordion"));

                    var name = modal.find(".name");
                    var major = modal.find("li:nth(0)");
                    var classes = modal.find("li:nth(1)");
                    var email = modal.find("li:nth(2)");
                    var about = modal.find("li:nth(3)");
                    var image = modal.find(".propic");
                    var link = modal.find(".collapseLink");
                    var pcollapse = modal.find(".panel-collapse");
                    var button = modal.find(".btn-primary");
                    var fade = modal.find(".fade");

                    name.html(people[idx].info_first_name + " " + people[idx].info_last_name);
                    major.html("Major: " + people[idx].info_major);

                    var coursesForBuddy = [];
                    for (var k = 0; k < people[idx].User.User_Courses.length; k++) {
                        var temp = people[idx].User.User_Courses[k].Course;
                        coursesForBuddy.push(temp.course_department + " " + temp.course_number);
                    }

                    classes.html("Courses: " + coursesForBuddy.join(", "));
                    email.html("Email: " + people[idx].User.user_email);
                    about.html("About Me: " + people[idx].info_bio);

                    image.attr("src", people[idx].info_picture);

                    link.attr("href", "#collapse" + idx);
                    pcollapse.attr("id", "collapse" + idx);

                    button.attr("data-target", "#myModal" + people[idx].id);
                    fade.attr("id", "myModal" + people[idx].id);
                }

            });

        });
    });

});