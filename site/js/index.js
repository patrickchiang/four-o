angular.module('FourO', ['ngRoute']).controller('IndexController', ['$scope', '$http', function ($scope, $http) {
    $scope.showAddClass = false;

    var me = $http.get('/userinfo/me');

    me.success(function (data, status, headers, config) {
        console.log(data);
        $scope.me = data;

        var coursesForBuddy = [];
        for (var k = 0; k < data.User.User_Courses.length; k++) {
            var temp = data.User.User_Courses[k].Course;
            coursesForBuddy.push(temp.course_department + " " + temp.course_number);
        }

        $scope.courses = coursesForBuddy.join(", ");

        $scope.addClass = function () {

            $http.post('/courses', {
                dept: $scope.dept,
                num: $scope.num
            }).success(function (data, status, headers, config) {
                $http.post('/users_courses/id', {
                    userId: $scope.me.UserId,
                    dept: $scope.dept,
                    num: $scope.num
                }).success(function (a, b, c, d) {
                    location.reload();
                });
            });
        }

        $scope.upload = function () {
            $("#theFile").click();
        }

        $("#theFile").change(function () {
            $("#fileSubmit").click();
        });
    });

    $scope.addButton = function () {
        $scope.showAddClass = !$scope.showAddClass;
    };


}]);