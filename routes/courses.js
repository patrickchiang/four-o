module.exports = function (app) {
    var models = require('../models');
    var ensure = require('./ensure.js');

    app.get('/courses', ensure, function (req, res) {
        models.Course.findAll().then(function (courses) {
            res.json(courses);
        });
    });

    app.post('/courses', function (req, res) {
        var dept = req.body.dept;
        var num = req.body.num;

        models.Course.find({
            where: models.Sequelize.and(
                {course_department: dept},
                {course_number: num}
            )
        }).then(function (courses) {

            console.log(courses);
            if (courses) {
                res.json("existing");
            } else {
                models.Course.create({
                    course_department: dept,
                    course_number: num
                }).then(function (course) {
                    course.save().then(function () {
                        res.json("success");
                    });
                });
            }
        });

    });

    app.get('/courses/dept/:dept', ensure, function (req, res) {
        models.Course.findAll({
            where: {course_department: {like: req.params.dept + "%"}}
        }).then(function (courses) {
            res.json(courses);
        });
    });

    app.get('/courses/search/:dept/:num', function (req, res) {
        models.Course.findAll({
            where: {
                course_department: {like: req.params.dept + "%"},
                course_number: {like: req.params.num + "%"}
            }
        }).then(function (courses) {
            res.json(courses);
        });
    });

};