module.exports = function (app) {
    var models = require('../models');
    var ensure = require('./ensure.js');

    app.post('/messages', function (req, res) {
        var sender = req.body.sender;
        var receiver = req.body.receiver;
        var text = req.body.text;

        models.Message.create({
            senderId: sender,
            receiverId: receiver,
            message_text: text
        }).then(function (message) {
            message.save().then(function () {
                res.json("success");
            });
        });
    });

    // list all messages in conversation
    app.get('/messages/:user1/:user2', function (req, res) {
        models.Message.findAll({
            where: models.Sequelize.and(
                models.Sequelize.or({
                    senderId: req.params.user1
                }, {
                    receiverId: req.params.user1
                }),
                models.Sequelize.or({
                    senderId: req.params.user2
                }, {
                    receiverId: req.params.user2
                })
            )
        }).then(function (messages) {
            res.json(messages);
        });
    });

    // list everyone this user has communicated with
    app.get('/messages/:user', function (req, res) {
        models.Message.findAll({
            where: models.Sequelize.or({
                senderId: req.params.user
            }, {
                receiverId: req.params.user
            }),
            attributes: ['senderId', 'receiverId']
        }).then(function (messages) {
            var targets = [];

            for (var i = 0; i < messages.length; i++) {
                if (messages[i].senderId != req.params.user) {
                    targets.push(messages[i].senderId);
                } else if (messages[i].receiverId != req.params.user) {
                    targets.push(messages[i].receiverId);
                }
            }

            res.json(uniq(targets));
        });
    });

    function uniq(a) {
        var seen = {};
        return a.filter(function (item) {
            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
    }

};