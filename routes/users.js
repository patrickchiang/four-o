module.exports = function (app) {
    var multiparty = require('multiparty');
    var fs = require('fs');

    var models = require('../models');
    var ensure = require('./ensure.js');

    app.get('/me', ensure, function (req, res) {
        models.User.find({
            where: {id: req.user.user_id}
        }).then(function (users) {
            res.json(users);
        });
    });

    app.get('/users', ensure, function (req, res) {
        models.User.findAll().then(function (users) {
            res.json(users);
        });
    });

    app.get('/users/id/:id', ensure, function (req, res) {
        models.User.find({
            where: {id: req.params.id}
        }).then(function (users) {
            res.json(users);
        });
    });

    app.get('/users/username/:username', ensure, function (req, res) {
        models.User.find({
            where: {user_username: req.params.username}
        }).then(function (users) {
            res.json(users);
        });
    });

    app.post('/userinfo/id/:id', function (req, res) {
        var firstName = req.body.first_name;
        var lastName = req.body.last_name;
        var bio = req.body.bio;
        var major = req.body.major;

        models.UserInfo.create({
            info_first_name: firstName,
            info_last_name: lastName,
            info_bio: bio,
            info_major: major,
            UserId: req.params.id
        }).then(function (userinfo) {
            userinfo.save().then(function () {
                res.json("success");
            });
        });
    });

    app.get('/userinfo/id/:id', ensure, function (req, res) {
        models.UserInfo.find({
            where: {UserId: req.params.id},
            include: {model: models.User, include: {model: models.User_Course, include: [models.Course]}}
        }).then(function (info) {
            res.json(info);
        });
    });

    app.get('/userinfo/me', ensure, function (req, res) {
        models.UserInfo.find({
            where: {UserId: req.user.user_id},
            include: {model: models.User, include: {model: models.User_Course, include: [models.Course]}}
        }).then(function (info) {
            res.json(info);
        });
    });

    app.post('/upload', ensure, function (req, res) {
        // parse a file upload
        var form = new multiparty.Form();

        form.parse(req, function (err, fields, files) {
            var user_id = req.user.user_id + "";
            var serverPath = '/site/images/' + user_id + "/" + files.theFile[0].originalFilename;

            if (!fs.existsSync('site/images/' + user_id)) {
                fs.mkdirSync('site/images/' + user_id);
            }

            fs.rename(files.theFile[0].path + "", __dirname + "/.." + serverPath, function (error) {
                if (error) {
                    console.log(error);
                    res.send({
                        error: 'Ah crap! Something bad happened'
                    });
                    return;
                }

                models.UserInfo.find({
                    where: {UserId: req.user.user_id}
                }).then(function (info) {
                    info.info_picture = '/images/' + user_id + "/" + files.theFile[0].originalFilename;
                    info.save().then(function () {
                        res.redirect("/");
                    });
                });

            });
        });
    });

    //app.get('/buddies', function (req, res) {
    //    models.sequelize.query('SELECT DISTINCT User.*, UserInfo.* FROM User_Course JOIN User ON User.id = User_Course.UserId JOIN UserInfo ON UserInfo.UserId = User_Course.UserId WHERE CourseId IN (SELECT User_Course.CourseId FROM User_Course WHERE User_Course.UserId = 1)')
    //        .then(function (data) {
    //            console.log(data);
    //            res.json(data);
    //        });
    //});

    function uniq(a) {
        var seen = {};
        return a.filter(function (item) {
            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
    }

};