module.exports = function (app) {
    var models = require('../models');
    var ensure = require('./ensure.js');

    app.post('/users_courses', function (req, res) {
        var userId = req.body.userId;
        var courseId = req.body.courseId;

        models.User_Course.create({
            UserId: userId,
            CourseId: courseId
        }).then(function (user_course) {
            user_course.save().then(function () {
                res.json("success");
            });
        });

    });

    app.post('/users_courses/id', function (req, res) {
        var userId = req.body.userId;
        var dept = req.body.dept;
        var num = req.body.num;

        models.Course.find({
            where: {
                "Course.course_department": dept,
                "Course.course_number": num
            }
        }).then(function (course) {
            console.log("test: " + course);

            models.User_Course.create({
                UserId: userId,
                CourseId: course.id
            }).then(function (user_course) {
                user_course.save().then(function () {
                    res.json("success");
                });
            });
        });

    });

    app.get('/users_courses', ensure, function (req, res) {
        models.User_Course.findAll().then(function (user_course) {
            res.json(user_course);
        });
    });

    // gets all users for a certain course except me
    app.get('/users_courses/users/:dept/:num', function (req, res) {
        models.User_Course.findAll({
            where: {
                "Course.course_department": {like: req.params.dept + "%"},
                "Course.course_number": {like: req.params.num + "%"},
                userId: {not: req.user.user_id}
            },
            attributes: ['userId'],
            include: [models.Course]
        }).then(function (courses) {
            var users = [];

            for (var i = 0; i < courses.length; i++) {
                users.push(courses[i].dataValues.userId);
            }

            res.json(uniq(users));
        });
    });

    // gets all courses for me
    app.get('/users_courses/courses', ensure, function (req, res) {
        models.User_Course.findAll({
            where: {
                "User.id": req.user.user_id
            },
            include: [models.User, models.Course]
        }).then(function (users) {
            var courses = [];

            for (var i = 0; i < users.length; i++) {
                var course = {
                    course_department: users[i].Course.course_department,
                    course_number: users[i].Course.course_number
                };

                courses.push(course);
            }

            res.json(courses);
        });
    });

    // gets all courses for a certain user
    app.get('/users_courses/courses/:id', ensure, function (req, res) {
        models.User_Course.findAll({
            where: {
                "User.id": req.params.id
            },
            include: [models.User, models.Course]
        }).then(function (users) {
            var courses = [];

            for (var i = 0; i < users.length; i++) {
                var course = {
                    course_department: users[i].Course.course_department,
                    course_number: users[i].Course.course_number
                };

                courses.push(course);
            }

            res.json(courses);
        });
    });

    function uniq(a) {
        var seen = {};
        return a.filter(function (item) {
            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
    }

};