module.exports = function (app) {

    var bcrypt = require('bcrypt-nodejs');
    var passport = require('passport'), LocalStrategy = require('passport-local').Strategy;
    var session = require('express-session');
    var bodyParser = require('body-parser');

    var models = require('../models');

    /**
     * Sessions setup
     */

    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());

    app.use(session({
        secret: 'patrick is the best as always',
        resave: true,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());

    /**
     * Passport serialization
     */

    passport.serializeUser(function (user, done) {
        done(null, user);
    });
    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

    /**
     * Passport local strategy users setup
     */

    passport.use('local-user', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'pass'
        },
        function (username, password, done) {
            models.User.find({
                where: models.Sequelize.or(
                    {user_username: username},
                    {user_email: username}
                )
            }).then(function (users) {
                var results = users;

                if (results == null)
                    return done(null, false);
                if (bcrypt.compareSync(password, results.user_password)) {
                    var user = results;
                    return done(null, {
                        user_id: user.id,
                        email: user.user_email
                    });
                } else {
                    return done(null, false);
                }
            }).error(function (err) {
                return done(null, false);
            });
        }
    ));

    /**
     * Routing
     */

    app.post('/login', passport.authenticate('local-user'), function (req, res) {
        res.json(req.user);
    });

    app.post('/register', function (req, res) {
        models.User.find({
            where: {
                user_email: req.body.email
            }
        }).then(function (model) {
            if (model != null) {
                console.log("already found");
                res.status(409).end();
            } else {

                // create the user
                models.User.create({
                    user_email: req.body.email,
                    user_password: bcrypt.hashSync(req.body.pass),
                    user_username: req.body.username
                }).then(function (user) {
                    user.save().then(function () {
                        models.UserInfo.create({
                            info_first_name: req.body.first_name,
                            info_last_name: req.body.last_name,
                            info_bio: req.body.aboutme,
                            info_major: req.body.major,
                            UserId: user.id
                        }).then(function (userInfo) {
                            userInfo.save().then(function () {
                                passport.authenticate('local-user')(req, res, function () {
                                    res.redirect('/');
                                });

                            });
                        });
                    });
                });
            }
        });
    });

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/login.html');
    });

};